plib.dbtools
============

The PLIB.DBTOOLS package provides an easier to use interface
to DB-API compliant databases.

Note: PLIB.DBTOOLS works with Python 2.7. If you are using
Python 3, see the PLIB3.DBTOOLS package, available at
https://gitlab.com/pdonis/plib3-dbtools.

The ``setup.py`` script for PLIB.DBTOOLS uses the ``setuputils``
helper module, which helps to automate away much of the
boilerplate in Python setup scripts. This module is available
as a separate release at https://gitlab.com/pdonis/setuputils.

The PLIB.DBTOOLS Package
------------------------

The following classes and functions are available in the ``plib.dbtools``
namespace:

- The ``DBInterface`` class provides a simple API for working with
  databases. It assumes that the underlying database engine conforms
  to the Python DB API. This class itself does not know about any
  particular database engine; a customized class should be derived
  from it for each particular engine.

- The ``MySQLDBInterface`` class customizes ``DBInterface`` to work
  with a MySQL database. It uses the ``MySQLdb`` third-party Python
  package. This class is in the ``plib.dbtools.mysql`` sub-package.

- The ``SQLite3DBInterface`` class customizes ``DBInterface`` to work
  with an SQLite version 3 database. It uses the ``sqlite3`` module
  that comes with Python as its database engine. This class is in the
  ``plib.dbtools.sqlite`` sub-package.

- The ``get_db_interface`` function is a convenience function that
  returns an instance of the appropriate ``DBInterface`` subclass for
  the database type passed to it.

- The ``get_db_interface_class`` and ``get_db_interface_args`` functions
  factor out key portions of ``get_db_interface`` so that they can be
  used separately if desired. For example, some databases might require
  subclassing the interface class further, or customizing the interface
  arguments.

Installation
------------

To install PLIB.DBTOOLS, you can simply run::

    $ python setup.py install

at a shell prompt from the directory into which you
unzipped the source tarball (the same directory that this
README file is in). This will install PLIB and then
run each of the post-install scripts in the scripts
directory.

The Zen of PLIB
---------------

There is no single unifying purpose or theme to PLIB, but
like Python itself, it does have a 'Zen' of sorts:

- Express everything possible in terms of built-in Python
  data structures.

- Once you've expressed it that way, what the code is
  going to do with it should be obvious.

- Avoid boilerplate code, *and* boilerplate data. Every
  piece of data your program needs should have one and
  only one source.

Copyright and License
---------------------

PLIB.DBTOOLS is Copyright (C) 2008-2020 by Peter A. Donis.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version. (See the LICENSE file for a
copy of version 2 of the License.)

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
