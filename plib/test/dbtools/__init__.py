#!/usr/bin/env python
"""
Sub-Package TEST.DBTOOLS of Package PLIB
Copyright (C) 2008-2020 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This sub-package contains the PLIB3.DBTOOLS test suite.
"""
