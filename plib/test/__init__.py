#!/usr/bin/env python
"""
Sub-Package TEST of Package PLIB
Copyright (C) 2008-2020 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This is a namespace package for the PLIB3 test suite.
"""

from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
